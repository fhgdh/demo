package com.example.demo.utils;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public class Utils {
    public static String getUuid() {

        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static String getOrderIdByUUId() {
        int machineId = 1;
        int hashCodev = UUID.randomUUID().toString().hashCode();
        if (hashCodev < 0) {
            hashCodev = -hashCodev;
        }
        return machineId + String.format("%015d", hashCodev);
    }

    /**
     * 获取传递过来url的参数
     *
     * @param req
     * @return
     */
    public static String getUrl(HttpServletRequest req) {
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String queryString = req.getQueryString();

        //将请求参数后面的pageCount参数截取掉
        if (queryString.contains("&pageNum=")) {
            int index = queryString.lastIndexOf("&pageNum=");
            queryString = queryString.substring(0, index);
        }
        return servletPath + "?" + queryString;
    }
}