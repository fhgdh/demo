package com.example.demo.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

@Controller
public class SkipController {

    @RequestMapping("/tomyorder")
    public String IndextoOrder(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        boolean success = false;
//        判断session里“user”是否为空
        if (request.getSession().getAttribute("user") == null) {
            System.out.println("------"+request.getSession().getAttribute("user")+"---跳转到我的订单--------");
            success = false;
        } else {
            success = true;
        }
        return success ? "myorder" : "redirect:/Test";
    }


}
