package net.suncaper.hostel.controller;

import javafx.scene.chart.ValueAxis;
import net.suncaper.hostel.domain.User;
import net.suncaper.hostel.service.UserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class usercontroller {
    @Autowired
    private UserService userService;

    @RequestMapping("/user-login")
    public String login() {
        return "user-login";
    }

    @RequestMapping("/admin-login")
    public String Adminlogin() {
        return "admin-login";
    }


    @RequestMapping("/update")
    public String toupdate(ModelMap modelMap, User user, HttpServletRequest request ) {
        {
                boolean success=true;
                String failMsg = null;
                User userDB= (User) request.getSession().getAttribute("loginuser");
                if (user.getName()==null)
                {
                    success=false;
                    failMsg="账号不能为空";
                }
                else {
                    userService.updataUser(user);
                }
            modelMap.put("content", "main");
            modelMap.put("failMsg", failMsg);
            return success?"index":"update";
        }
    }
//            boolean success = true;
//            String failMsg = null;
//            if (user.getName() == null) {
//                success = false;
//                failMsg = "用户名错误";
//            } else {
//                userService.updataUser(user);
//            }
//            modelMap.put("content", "main");
//            modelMap.put("failMsg", failMsg);
//            return success ? "index" : "update";
//
//        }



    @RequestMapping("/login")
    public String tologin(ModelMap modelMap, User user, HttpServletRequest request) {
        boolean success = true;
        String failMsg = null;
        User userDB = userService.findUserbyName(user.getName());
        if (userDB == null) {
            success = false;
            failMsg = "用户名不存在";
        } else if (!(userDB.getPwd().equals(user.getPwd()))) {
            success = false;
            failMsg = "密码错误";
        }
        if (success) {
           request.getSession().setAttribute("loginuser",user);
        }
        modelMap.put("content", "main");
        modelMap.put("failMsg", failMsg);
        System.out.println(userDB.getName());
        return success ? "index" : "update";

    }

    @RequestMapping("/register")
    public String toregister(ModelMap modelMap, User user,@RequestParam("pwd2")String pwd2) {
        boolean success = true;
        String failMsg1 = null;
        if(user.getName()==null||"".equals(user.getName().trim())){
            failMsg1="填写的账号为空!";
            modelMap.put("content","main");
            modelMap.put("failMsg1",failMsg1);
            return "user-login";
        }
        if(user.getPwd()==null||pwd2==null
                ||"".equals(user.getPwd().trim())||"".equals(pwd2.trim())
                ||!user.getPwd().equals(pwd2)){
            failMsg1="填写的两次密码不正确!";
            modelMap.put("content","main");
            modelMap.put("failMsg1",failMsg1);
            return  "user-login";
        }
        if(user.getNickname()==null||"".equals(user.getNickname().trim())){
            failMsg1="填写的用户名为空!";
            modelMap.put("content","main");
            modelMap.put("failMsg1",failMsg1);
            return "user-login";
        }
        if(user.getEmail()==null||"".equals(user.getEmail().trim())){
            failMsg1="填写的邮箱为空!";
            modelMap.put("content","main");
            modelMap.put("failMsg1",failMsg1);
            return "user-login";
        }
        if(user.getTel()==null){
            failMsg1="填写的电话号码为空!";
            modelMap.put("content","main");
            modelMap.put("failMsg1",failMsg1);
            return "user-login";
        }
        if(userService.findUserbyName(user.getName())!=null){
            failMsg1="填写的用户名已存在";
            modelMap.put("content","main");
            modelMap.put("failMsg1",failMsg1);
            return "user-login";
        }
        userService.insertUser(user);
        return "index";
    }
}

