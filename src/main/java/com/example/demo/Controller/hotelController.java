package com.example.demo.Controller;

import com.example.demo.mapper.HotelMapp;
import com.example.demo.bean.hotel01;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = "hotel")
public class  hotelController {List<hotel01> list;
    @Autowired
    private HotelMapp hotelmapp;
    List<hotel01> list;
    @RequestMapping(value = "/hotelS")
    @ResponseBody     //进行酒店搜索，根据页面传的值将符合条件的酒店搜索出来封装为List集合。
    public String findhotel(@RequestParam(value = "city") String city, @RequestParam(value = "hotel_translated_name") String hotel_translated_name, Model model){
        List<hotel01> hotels=hotelmapp.find(city,'%'+hotel_translated_name+'%');
//        List<hotel> hotels1=hotelmapper.selectAll();
//        System.out.println(hotels1.size());
//        System.out.println(hotels.size());
//        for (hotel s:hotels){
//            System.out.println(s.getHotel_translated_name());
//        }
        list=hotels;

        return "a";
    }

    @RequestMapping(value = "/show1")  //将查询出的酒店List集合转发给列表页面。
    public String ddd(Model model){
        model.addAttribute("hotels",list);
//        for (hotel s:list){
//            System.out.println(s.getHotel_translated_name());
//        }
        return "showhotellist/showList";
    }
    //返回首页。
    @RequestMapping(value = "/inde")
    public String inde(){
        return "index";
    }

}
