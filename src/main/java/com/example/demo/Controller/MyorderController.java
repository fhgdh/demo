package com.example.demo.Controller;

import com.example.demo.bean.Order;
import com.example.demo.bean.Hotel;
import com.example.demo.bean.Room;
import com.example.demo.bean.User;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.service.MyorderService;
import com.example.demo.service.impl.OrderServiceimpl;
import com.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("")
public class MyorderController {
    @Autowired
    MyorderService myorderService;
    @Autowired
    OrderMapper orderMapper;

    @RequestMapping("/myorder")
    public String orderPage(@RequestParam(value = "userId", defaultValue = "default") String userId, ModelMap modelMap, HttpServletRequest request) {
           /* if(request.getSession().getAttribute("user")==null){
                return "index";
            }*/
        int id = 0;
        User user = new User();
        if (userId == null || "default".equals(userId)) {
            return "redirect:/index";
        } else {
            id = Integer.parseInt(userId);
            user.setUserId(id);
        }
        User user2= (User) request.getSession().getAttribute("user");
        System.out.println("____________________________");
        System.out.println(userId);
        System.out.println("_______________________________");
        List<Order> orders = myorderService.getOrderInfo(user.getUserId());
        Order order1 = orders.get(0);
        System.out.println("__________测试UserId赋值___________________");
      if (order1.getOrderId()!=null){
          modelMap.addAttribute("userId", order1.getUserId());
          System.out.println(order1.getUserId()+"这个是页面传递的ID");
      }
      else {
          modelMap.addAttribute("userId", user2.getUserId());
          System.out.println(user2.getUserId()+"这个是Session的ID");
      }
        System.out.println("__________测试UserId赋值___________________");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
       /* for (Order order:orders)
        {
            order.setTime( simpleDateFormat2.parse(order.getTime().toString()));
        }*/
        modelMap.addAttribute("orders", orders);
        return "myorder";
    }

    @RequestMapping("/ordermsg")
    public String ToMyOrderMsg(@RequestParam(value = "orderId", defaultValue = "default") String orderId, Model model, HttpServletRequest request) {
           /* if(request.getSession().getAttribute("user")==null){
                return "index";
            }*/
        int id = 0;
        Order order = new Order();
        if (orderId == null || "default".equals(orderId)) {
            return "redirect:/index";
        } else {
            id = Integer.parseInt(orderId);
            order.setOrderId(id);
            System.out.println(orderId);
            System.out.println(order.getOrderId());
        }
        Order order1 = myorderService.getMyorder(order.getOrderId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String start = simpleDateFormat.format(order1.getStartdate());
        String time = simpleDateFormat2.format(order1.getTime());
        System.out.println(order1.toString());
        model.addAttribute("hotalName", order1.getHotelTranslatedName());
        System.out.println("添加酒店名");
        model.addAttribute("roomName", order1.getRoomName());
        System.out.println("添加房间名");
        model.addAttribute("name", order1.getLivePeople());
        model.addAttribute("phone", order1.getPhone());
        model.addAttribute("bedType", order1.getBedType());
        model.addAttribute("startData", start);
        System.out.println("添加下单时间");
        model.addAttribute("time",time);
        System.out.println("添加订单生成时间");
        model.addAttribute("ordernum", order1.getOrdernum());
        System.out.println("添加订单编号");
        model.addAttribute("state", order1.getState());
        model.addAttribute("userId", order1.getUserId());
        model.addAttribute("orderId", order1.getOrderId());
        return "ordermsg";
    }

    @RequestMapping("/order")
    public String ordertopay(@RequestParam(value = "orderId", defaultValue = "default") String orderId, Model model, HttpServletRequest request) {
           /* if(request.getSession().getAttribute("user")==null){
                return "index";
            }*/
        int id = 0;
        Order order = new Order();
        if (orderId == null || "default".equals(orderId)) {
            return "redirect:/index";
        } else {
            id = Integer.parseInt(orderId);
            order.setOrderId(id);
           /* System.out.println(orderId);
            System.out.println(order.getOrderId());*/
        }
        Order order1 = myorderService.getMyorder(order.getOrderId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String start = simpleDateFormat.format(order1.getStartdate());
        String last = simpleDateFormat.format(order1.getLastdate());
        System.out.println(order1.toString());
        model.addAttribute("hotelName", order1.getHotelTranslatedName());
        model.addAttribute("roomName", order1.getRoomName());
        model.addAttribute("name", order1.getLivePeople());
        model.addAttribute("phone", order1.getPhone());
        model.addAttribute("bedType", order1.getBedType());
        model.addAttribute("startDate", start);
        model.addAttribute("lastDate", last);
        model.addAttribute("state", order1.getState());
        model.addAttribute("userId", order1.getUserId());
        model.addAttribute("fprice", order1.getFprice());
        model.addAttribute("orderId", orderId);
        model.addAttribute("orderNum", order1.getOrdernum());
        return "orderconfirm";
    }

    @RequestMapping("/toMain")
    public String ToMain(@RequestParam(value = "userId", defaultValue = "default") String userId, ModelMap modelMap, HttpServletRequest request) {
           /* if(request.getSession().getAttribute("user")==null){
                return "index";
            }*/
        int id = 0;
        User user = new User();
        id = Integer.parseInt(userId);
        user.setUserId(id);
        System.out.println("__________________去首页的下划线__________");
        System.out.println("____________________________");
        System.out.println(userId);
        User user1 = myorderService.getUserallInfo(user.getUserId());

        System.out.println("_______________________________");
        System.out.println(user1.getUserId());
        System.out.println(user1.getNickname());
        System.out.println(user1.getPwd());
        System.out.println("____________________________");
        request.getSession().setAttribute("user",user1);
        return "/index";
    }
     @RequestMapping("/myorder_state")
    public String MyorderState(@RequestParam(value = "order_id") Integer orderId,Model model){
        Order order= orderMapper.selectByPrimaryKey(orderId);
        order.setState("已取消");
        orderMapper.updateByPrimaryKeySelective(order);
        model.addAttribute("state","已取消");
        model.addAttribute("userId",order.getUserId());
        System.out.println("x");
        return "ordermsg::my_state";
    }
    @RequestMapping("/myorder_state_change")
    public String MyorderStateChange(@RequestParam(value = "order_id") Integer orderId,Model model){
        Order order= orderMapper.selectByPrimaryKey(orderId);
        order.setState("已申请");
        orderMapper.updateByPrimaryKeySelective(order);
        model.addAttribute("state","已申请");
        model.addAttribute("userId",order.getUserId());
        return "ordermsg::my_state";
    }

    @RequestMapping("/myorder_change")
    public String MyorderChange(@RequestParam(value = "user_Id") Integer orderId,Model model){
        Order order= orderMapper.selectByPrimaryKey(orderId);
        order.setState("已申请");
        System.out.println("_-________________--");
        orderMapper.updateByPrimaryKeySelective(order);
        model.addAttribute("state","已申请");
        model.addAttribute("userId",order.getUserId());
        return "myorder::my_state";
    }



   /* @RequestParam("/message")
    public  toMessage (@RequestParam(value = ""))*/
}