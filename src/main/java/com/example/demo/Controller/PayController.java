package com.example.demo.Controller;

import com.example.demo.bean.Order;
import com.example.demo.bean.Hotel;
import com.example.demo.bean.Room;
import com.example.demo.bean.User;
import com.example.demo.service.MyorderService;
import com.example.demo.service.impl.OrderServiceimpl;
import com.example.demo.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Controller
@RequestMapping("")
public class PayController {
    @Autowired
    private OrderServiceimpl orderService;
    @Autowired
    MyorderService myorderService;

    @RequestMapping("/orderpay")
    public String orderPage(@RequestParam(value = "roomId", defaultValue = "default") String roomId,
                            ModelMap modelMap, HttpServletRequest request) {

        System.out.println("______orderPage : roomId=" + roomId);
            if(request.getSession().getAttribute("user")==null){
                return "user-login";
            }
        int id = 0;
        Room room = new Room();
        User user = (User) request.getSession().getAttribute("user");
        int q=user.getUserId();
        System.out.println("_________________________________________"+q);
        if (roomId == null || "default".equals(roomId)) {
            return "redirect:/ordermsg";
        } else {
            id = Integer.parseInt(roomId);
            room.setRoomId(id);
        }
        List<Room> rooms = orderService.getRoomInfo(room);
        Room room1 = rooms.get(0);
        String roomType = "";
        Hotel hotelInfo = orderService.getHotelInfo(room1.getHotelId());
        modelMap.addAttribute("bedType", room1.getBedType());
        modelMap.addAttribute("photo", room1.getPhoto1());
        modelMap.addAttribute("price", room1.getPrice());
        modelMap.addAttribute("room", rooms.get(0));
        modelMap.addAttribute("roomName",room1.getName());
        modelMap.addAttribute("hotelName", hotelInfo.getHotelTranslatedName());
        modelMap.addAttribute("hotalId",room1.getHotelId());
        modelMap.addAttribute("userId",user.getUserId());
        return "orderpay";
    }


    @RequestMapping("/orderconfirm")
    public String orderPay(@RequestParam(value = "roomId", defaultValue = "default") String roomId,@RequestParam(value = "userId") int userId,
                           @RequestParam(value = "bedType", defaultValue = "default") String bedType, @RequestParam(value = "name", defaultValue = "default") String name,
                           @RequestParam(value = "roomName", defaultValue = "default") String roomName,@RequestParam(value = "orderPrice", defaultValue = "default") String orderPrice,
                           @RequestParam(value = "startdate", defaultValue = "default") String startdate, @RequestParam(value = "lastdate", defaultValue = "default") String lastdate,
                           @RequestParam(value = "hotelName", defaultValue = "default", required = false) String hotelName, @RequestParam(value = "phone", defaultValue = "default") String phone,
                           @RequestParam(value = "hotelId", defaultValue = "default") String hotelId, @RequestParam(value = "fprice", defaultValue = "default") String fprice,
                           @RequestParam(value = "roomCount", defaultValue = "1") String roomCount,
                           ModelMap modelMap, HttpServletRequest request) throws ParseException {
        int id = 0;
        System.out.println("orderPay Controller");
           /* if(request.getSession().getAttribute("user")==null){
                System.out.println("return login");
                return "redirect:/login";
            }*/
        if (roomId == null || "default".equals(roomId)) {
            System.out.println("return index");
            return "redirect:/";
        }
        System.out.println("测试：");
        System.out.println("orderPrice =  " + orderPrice + "  endTime=  " + lastdate + "  roomCount=" + roomCount + "  phone  = " + phone + "  name =  " + name);
        System.out.println("bedType =  " + bedType + "  roomId=  " + roomId + "  fprice=" + fprice + "  hotelName  = " + hotelName);
        System.out.println("hotelId = " + hotelId + "  startTime = " + startdate + "  name = " + name+ "  roomName = " + roomName);

        if (bedType == null || startdate == null || lastdate == null || name == null || phone == null || hotelId == null || fprice == null || roomCount == null ||
                "default".equals(bedType) || "default".equals(name) || "default".equals(phone) ||
                "default".equals(name) || "default".equals(hotelId)) {
            request.setAttribute("roomId", Integer.parseInt(roomId));
            System.out.println("return order");
            return "redirect:/orderpay?roomId=" + roomId;
        }
        Order order = new Order();
           String orderNum = Utils.getOrderIdByUUId();
           User user = (User) request.getSession().getAttribute("user");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String start = simpleDateFormat.format(new Date());
        String last = simpleDateFormat.format(new Date());
        Date startDate=   simpleDateFormat.parse(start);
        Date lastDate=   simpleDateFormat.parse(last);
        BigDecimal price = new BigDecimal(fprice);
        order.setUserId(userId);
        order.setRoomId(Integer.parseInt(roomId));
        order.setStartdate(startDate);
        order.setLastdate(lastDate);
        order.setFprice(price);
        //2  未支付
        order.setState("未支付");;
               order.setLivePeople(name);
               order.setPhone(phone);
              order.setOrdernum(orderNum);
        order.setTime(new Date());
        order.setRoomName(roomName);
        order.setHotelTranslatedName(hotelName);
        order.setBedType(bedType);
        //向数据库插入订单信息
        orderService.insertOrder(order);
        request.getSession().setAttribute("fprice", fprice);
        modelMap.addAttribute("name", name);
        modelMap.addAttribute("bedType", bedType);
        modelMap.addAttribute("startDate", start);
        modelMap.addAttribute("lastDate", last);
        modelMap.addAttribute("hotelName", hotelName);
        modelMap.addAttribute("roomName",roomName);
        modelMap.addAttribute("name",name);
        modelMap.addAttribute("phone", phone);
        modelMap.addAttribute("fprice", fprice);
        modelMap.addAttribute("userId", userId);
        modelMap.addAttribute("orderNum", orderNum);
        return "/orderconfirm";
    }

}
