package com.example.demo.Controller;


import com.example.demo.Bean.Order;
import com.example.demo.Bean.OrderExample;
import com.example.demo.Mapper.OrderMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("torder")
public class OrderController {
    @Autowired
    OrderMapper torderMapper;

    @RequestMapping("/torders")
    public String Torders(@RequestParam(value = "user_id") String user_id, Model model){
        List<Order> torders;
        OrderExample example=new OrderExample();
        example.createCriteria().andUserIdEqualTo(Integer.parseInt(user_id));
        torders=torderMapper.selectByExample(example);
        model.addAttribute("torders",torders);
        return "order/orders";
    }
    @RequestMapping("/showOrder")
    public String showorder(@RequestParam(value = "orderId") String orderId,Model model){
        Order torder;
        torder=torderMapper.selectByPrimaryKey(Integer.parseInt(orderId));
        model.addAttribute("torder",torder);
        return "order/showorder";
    }

}
