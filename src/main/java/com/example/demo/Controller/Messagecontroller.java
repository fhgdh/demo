package net.suncaper.hostel.controller;

import com.sun.org.apache.xpath.internal.operations.Mod;
import net.suncaper.hostel.domain.Message;
import net.suncaper.hostel.domain.User;
import net.suncaper.hostel.service.MessageService;
import net.suncaper.hostel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class Messagecontroller {
    @Autowired
    MessageService messageService;

    @RequestMapping("evalute")
    public  String toevalute(@RequestParam(value = "hotelId")int hotelId, ModelMap modelMap, User user, HttpServletRequest request)
    {
        List<Message>  messages=messageService.findMessagebyhotel(hotelId);
        request.getSession().setAttribute("hotel_id",hotelId);
        modelMap.addAttribute("messages",messages);
        return "evalute";
    }
    @RequestMapping("evalutepage")
    public String toinsert(@RequestParam(value = "message") String message,@RequestParam(value = "room_name") String room_name,HttpServletRequest request)
    {
        User userDB=(User)request.getSession().getAttribute("loginuser");
        int hotelId=(int)request.getSession().getAttribute("hotel_id");
        messageService.insertMessage(hotelId,message,userDB.getName(),room_name);
        return "index";
    }

}
