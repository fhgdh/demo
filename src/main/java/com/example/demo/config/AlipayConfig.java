


package com.example.demo.config;

public class AlipayConfig {
	// 商户appid
	public static String APPID = "2016101200665750";
	// 私钥 pkcs8格式的
	public static String RSA_PRIVATE_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC6HLImh7026zUfMHtFUOj36wM69LHjq7kbe7j+XnY/qgrS1QRKkM4lxEW6AaJgsUZmjDrIqpyaVppGi7B7vXnDVZTZ5fuo2rC/KPn0W5gvjR6cHwuCvs7TsyMvmlDsHr9/R3q3TjpLq6bJqexngrYQCjOndvconpjKAbgAud01jC+cZaCo/fLNXXAO7OsdynRav2cUSf37OBw4xwiRsxrgyGvtCDC0GpOmhSGZaCP5vNso8yCRg3lQ73/Pqo4bZNB/Gy8X5qAOwcVo0zbezOF9BebOmvEv0HuKemBw6DnmN08ymMYM1qZjHkMMmPvKg5TO3CxEDPgpgYyDDFCwFgZVAgMBAAECggEAUCLpBrQt4gbXAc4DVi5nkdCMvIb+TdT0SIQ2fLi52+Hg9TUput2tFg/0IgpwG4hZkvBQo3TrJ32gwj2FyVIlqtkf1c80VFmN/1GqhvAlkFwIURsA/tQgBIG+5dFhhLocpLZnfaReb5TCpVTYaR2cPT3XTgVomQHpMhoZW56sRFCTeIh/Do9FDT1Thu80x8brcO2whFmUiWiSmCXQn7hb7ciTzlSXbBXYVdmuBGKNUclrQo5gqX4Q8iPJ9cj4LrzVFiXASfX7PpJkeEiVLeVIv+bYkauLbxjwZR0G4xEvIsQhBm+xul9sD0Hv3IC4l0frrioKqLYuy3WcehLThTSgBQKBgQDcDv72tiMSkeiKZKWd4dA/mI7qWwt1WSGxNLrdeY+wJGtP0jYybXcw5T35RjfRxRTDXs+rRGXFy2LIsyKZyUakaipD94Ft1CVXP44HUqAbh9HDnGlxpW+ElE91lpEUWkjYiR5D2mGEfuFV9srMg0Nae8GQWoq+Mc06VCXMsSS/8wKBgQDYgle55YQt5GYz6G67E7gPlVGPL0AtQIJbAaYaOYqO/rFfksZBp+ecmabZYXQlNLtHklA1OkIMY16AieuzQn7z7UoOFUxDxy735yBlHOBowq+XvPTFG20P6KoyOH8+HR/4hWuXx07WAgxc9wc6WueyC7XpmyycPNjDcI9kixV6lwKBgF6rTOCSJrB8eM/g0KcFWHOjjAgiNuG7AJWdm65f94LNiCUEyDAVi6tLU8gDf0wNPwt1pF2HxUi8YLj6563FO1DMI3at6c5378CDJPTdhOeTHJjMl0g+5aThsPVjWvKojKIrCwwhzMYVV5eF3mTxK26nwvP5H6sG/cVUIhbXNnolAoGBALVvRSS7ebEbnaoSNfj/5VbpwpdC+XzyekrZbuC7J6n9NZbp4MpUcnuBJ5kzWal0Lx0jN6PjVkYbX65H5gUqzEiNqWlz351M5cdJIJSbRlOEErIN3vDuss6KI1ZCkh7zOT+CT0+57g7ZWJLvUFdOpqtFtQQ2zYOAWgYt5XxUMYOxAoGBAM6qcpDjel30SHjrUE+Vs8kz6bISM4bEb6drulr/qeoDMqKUVGufrQwLxNICs30T2tis60Zh09NUh2LwCsLT76Pz+lQPumFpaXgFf3ajt1rVlM+KzU8BRCoRpwYWAF5H04mWoevfp9EjHnihOGm2EyWw+UJYTrokHD+yOb3gdXT9";
	// 服务器异步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080/alipay.trade.wap.pay-java-utf-8/notify_url.jsp";
	// 页面跳转同步通知页面路径 需http://或者https://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问 商户可以自定义同步跳转地址
	public static String return_url = "http://localhost:8080/payReturn";
	// 请求网关地址                                                                                                                           
	public static String URL = "https://openapi.alipaydev.com/gateway.do";
	// 编码
	public static String CHARSET = "UTF-8";
	// 返回格式
	public static String FORMAT = "json";
	// 支付宝公钥
	public static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoogF9KTHNtqaynBNAdk9EHCTCSeglXwD8OF0VPCdVWDJUXPxq8UZvHANe/gjbTPkof5Pl1kdykL2iwYUnv3au5HOa6XEYw0EBcOUQtrEAOiPYICz7NWOQ7lsgB/d2o/6ChryATFyJoPfeD41izhQXIIXte4cWF2kArDqxTRd2QO/QjYuPl3LqXy7LmDeDdTQpi4perCRtXlMP0YLtOFkYeJp+SVf6UtdbuO17wzlva7f6JPOItH5k7VHVXAKaMkwiM1QBVC6jLCAqxCoqTXhHgmZ6cwW2hGnp/j3Zp9JG+7m0WXcUVguUlngaP9NNBbctjsSjFluD1WutcCtE3/xkwIDAQAB" + 
			"";
	// 日志记录目录
	public static String log_path = "/log";
	// RSA2
	public static String SIGNTYPE = "RSA2";
}
