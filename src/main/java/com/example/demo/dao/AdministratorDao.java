package com.example.demo.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository("AdministratorDao")
@Mapper
public interface AdministratorDao
{
    List<Map> findLike(String keyWords);

    int agree(int order_id);
    int disagree(int order_id);

    List<Map> allRoom();
    public int updateById(Map map);
}

