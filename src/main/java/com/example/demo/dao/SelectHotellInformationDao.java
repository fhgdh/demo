package com.example.demo.dao;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


@Repository("SelectHotellInformationDao")
@Mapper
public interface SelectHotellInformationDao {
	List<Map> selectRoomByHotellID(Integer hotel_id);

	List<Map> selectRoom(Map map);
}
