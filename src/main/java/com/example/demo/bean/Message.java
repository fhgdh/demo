package net.suncaper.hostel.domain;

public class Message {
    private Integer mId;

    private String message;

    private Integer hotelId;

    private String messageUserName;

    private String messageRoomName;

    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    public Integer getHotelId() {
        return hotelId;
    }

    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    public String getMessageUserName() {
        return messageUserName;
    }

    public void setMessageUserName(String messageUserName) {
        this.messageUserName = messageUserName == null ? null : messageUserName.trim();
    }

    public String getMessageRoomName() {
        return messageRoomName;
    }

    public void setMessageRoomName(String messageRoomName) {
        this.messageRoomName = messageRoomName == null ? null : messageRoomName.trim();
    }
}