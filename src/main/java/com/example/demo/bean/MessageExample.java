package net.suncaper.hostel.domain;

import java.util.ArrayList;
import java.util.List;

public class MessageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MessageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMIdIsNull() {
            addCriterion("m_id is null");
            return (Criteria) this;
        }

        public Criteria andMIdIsNotNull() {
            addCriterion("m_id is not null");
            return (Criteria) this;
        }

        public Criteria andMIdEqualTo(Integer value) {
            addCriterion("m_id =", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotEqualTo(Integer value) {
            addCriterion("m_id <>", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdGreaterThan(Integer value) {
            addCriterion("m_id >", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("m_id >=", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdLessThan(Integer value) {
            addCriterion("m_id <", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdLessThanOrEqualTo(Integer value) {
            addCriterion("m_id <=", value, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdIn(List<Integer> values) {
            addCriterion("m_id in", values, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotIn(List<Integer> values) {
            addCriterion("m_id not in", values, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdBetween(Integer value1, Integer value2) {
            addCriterion("m_id between", value1, value2, "mId");
            return (Criteria) this;
        }

        public Criteria andMIdNotBetween(Integer value1, Integer value2) {
            addCriterion("m_id not between", value1, value2, "mId");
            return (Criteria) this;
        }

        public Criteria andMessageIsNull() {
            addCriterion("message is null");
            return (Criteria) this;
        }

        public Criteria andMessageIsNotNull() {
            addCriterion("message is not null");
            return (Criteria) this;
        }

        public Criteria andMessageEqualTo(String value) {
            addCriterion("message =", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageNotEqualTo(String value) {
            addCriterion("message <>", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageGreaterThan(String value) {
            addCriterion("message >", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageGreaterThanOrEqualTo(String value) {
            addCriterion("message >=", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageLessThan(String value) {
            addCriterion("message <", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageLessThanOrEqualTo(String value) {
            addCriterion("message <=", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageLike(String value) {
            addCriterion("message like", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageNotLike(String value) {
            addCriterion("message not like", value, "message");
            return (Criteria) this;
        }

        public Criteria andMessageIn(List<String> values) {
            addCriterion("message in", values, "message");
            return (Criteria) this;
        }

        public Criteria andMessageNotIn(List<String> values) {
            addCriterion("message not in", values, "message");
            return (Criteria) this;
        }

        public Criteria andMessageBetween(String value1, String value2) {
            addCriterion("message between", value1, value2, "message");
            return (Criteria) this;
        }

        public Criteria andMessageNotBetween(String value1, String value2) {
            addCriterion("message not between", value1, value2, "message");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNull() {
            addCriterion("hotel_id is null");
            return (Criteria) this;
        }

        public Criteria andHotelIdIsNotNull() {
            addCriterion("hotel_id is not null");
            return (Criteria) this;
        }

        public Criteria andHotelIdEqualTo(Integer value) {
            addCriterion("hotel_id =", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotEqualTo(Integer value) {
            addCriterion("hotel_id <>", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThan(Integer value) {
            addCriterion("hotel_id >", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("hotel_id >=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThan(Integer value) {
            addCriterion("hotel_id <", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdLessThanOrEqualTo(Integer value) {
            addCriterion("hotel_id <=", value, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdIn(List<Integer> values) {
            addCriterion("hotel_id in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotIn(List<Integer> values) {
            addCriterion("hotel_id not in", values, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdBetween(Integer value1, Integer value2) {
            addCriterion("hotel_id between", value1, value2, "hotelId");
            return (Criteria) this;
        }

        public Criteria andHotelIdNotBetween(Integer value1, Integer value2) {
            addCriterion("hotel_id not between", value1, value2, "hotelId");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameIsNull() {
            addCriterion("message_user_name is null");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameIsNotNull() {
            addCriterion("message_user_name is not null");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameEqualTo(String value) {
            addCriterion("message_user_name =", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameNotEqualTo(String value) {
            addCriterion("message_user_name <>", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameGreaterThan(String value) {
            addCriterion("message_user_name >", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("message_user_name >=", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameLessThan(String value) {
            addCriterion("message_user_name <", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameLessThanOrEqualTo(String value) {
            addCriterion("message_user_name <=", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameLike(String value) {
            addCriterion("message_user_name like", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameNotLike(String value) {
            addCriterion("message_user_name not like", value, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameIn(List<String> values) {
            addCriterion("message_user_name in", values, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameNotIn(List<String> values) {
            addCriterion("message_user_name not in", values, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameBetween(String value1, String value2) {
            addCriterion("message_user_name between", value1, value2, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageUserNameNotBetween(String value1, String value2) {
            addCriterion("message_user_name not between", value1, value2, "messageUserName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameIsNull() {
            addCriterion("message_room_name is null");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameIsNotNull() {
            addCriterion("message_room_name is not null");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameEqualTo(String value) {
            addCriterion("message_room_name =", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameNotEqualTo(String value) {
            addCriterion("message_room_name <>", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameGreaterThan(String value) {
            addCriterion("message_room_name >", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameGreaterThanOrEqualTo(String value) {
            addCriterion("message_room_name >=", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameLessThan(String value) {
            addCriterion("message_room_name <", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameLessThanOrEqualTo(String value) {
            addCriterion("message_room_name <=", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameLike(String value) {
            addCriterion("message_room_name like", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameNotLike(String value) {
            addCriterion("message_room_name not like", value, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameIn(List<String> values) {
            addCriterion("message_room_name in", values, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameNotIn(List<String> values) {
            addCriterion("message_room_name not in", values, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameBetween(String value1, String value2) {
            addCriterion("message_room_name between", value1, value2, "messageRoomName");
            return (Criteria) this;
        }

        public Criteria andMessageRoomNameNotBetween(String value1, String value2) {
            addCriterion("message_room_name not between", value1, value2, "messageRoomName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}