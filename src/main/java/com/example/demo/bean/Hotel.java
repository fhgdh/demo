package com.example.demo.bean;

public class Hotel {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.hotel_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private Integer hotelId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.chain_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String chainId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.chain_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String chainName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.brand_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String brandId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.brand_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String brandName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.hotel_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String hotelName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.hotel_formerly_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String hotelFormerlyName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.hotel_translated_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String hotelTranslatedName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.addressline1
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String addressline1;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.addressline2
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String addressline2;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.zipcode
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String zipcode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.city
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String city;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.state
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String state;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.country
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String country;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.countryisocode
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String countryisocode;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.star_rating
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String starRating;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.longitude
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String longitude;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.latitude
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String latitude;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.url
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String url;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.checkin
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String checkin;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.checkout
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String checkout;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.numberrooms
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String numberrooms;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.numberfloors
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String numberfloors;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.yearopened
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String yearopened;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.yearrenovated
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String yearrenovated;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.photo1
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String photo1;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.photo2
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String photo2;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.photo3
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String photo3;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.photo4
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String photo4;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.photo5
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String photo5;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.overview
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String overview;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.rates_from
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String ratesFrom;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.continent_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String continentId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.continent_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String continentName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.city_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String cityId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.country_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String countryId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.number_of_reviews
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String numberOfReviews;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.rating_average
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String ratingAverage;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_hotelticket.rates_currency
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    private String ratesCurrency;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.hotel_id
     *
     * @return the value of t_hotelticket.hotel_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public Integer getHotelId() {
        return hotelId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.hotel_id
     *
     * @param hotelId the value for t_hotelticket.hotel_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setHotelId(Integer hotelId) {
        this.hotelId = hotelId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.chain_id
     *
     * @return the value of t_hotelticket.chain_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getChainId() {
        return chainId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.chain_id
     *
     * @param chainId the value for t_hotelticket.chain_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setChainId(String chainId) {
        this.chainId = chainId == null ? null : chainId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.chain_name
     *
     * @return the value of t_hotelticket.chain_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getChainName() {
        return chainName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.chain_name
     *
     * @param chainName the value for t_hotelticket.chain_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setChainName(String chainName) {
        this.chainName = chainName == null ? null : chainName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.brand_id
     *
     * @return the value of t_hotelticket.brand_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getBrandId() {
        return brandId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.brand_id
     *
     * @param brandId the value for t_hotelticket.brand_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setBrandId(String brandId) {
        this.brandId = brandId == null ? null : brandId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.brand_name
     *
     * @return the value of t_hotelticket.brand_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.brand_name
     *
     * @param brandName the value for t_hotelticket.brand_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName == null ? null : brandName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.hotel_name
     *
     * @return the value of t_hotelticket.hotel_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getHotelName() {
        return hotelName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.hotel_name
     *
     * @param hotelName the value for t_hotelticket.hotel_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setHotelName(String hotelName) {
        this.hotelName = hotelName == null ? null : hotelName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.hotel_formerly_name
     *
     * @return the value of t_hotelticket.hotel_formerly_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getHotelFormerlyName() {
        return hotelFormerlyName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.hotel_formerly_name
     *
     * @param hotelFormerlyName the value for t_hotelticket.hotel_formerly_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setHotelFormerlyName(String hotelFormerlyName) {
        this.hotelFormerlyName = hotelFormerlyName == null ? null : hotelFormerlyName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.hotel_translated_name
     *
     * @return the value of t_hotelticket.hotel_translated_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getHotelTranslatedName() {
        return hotelTranslatedName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.hotel_translated_name
     *
     * @param hotelTranslatedName the value for t_hotelticket.hotel_translated_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setHotelTranslatedName(String hotelTranslatedName) {
        this.hotelTranslatedName = hotelTranslatedName == null ? null : hotelTranslatedName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.addressline1
     *
     * @return the value of t_hotelticket.addressline1
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getAddressline1() {
        return addressline1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.addressline1
     *
     * @param addressline1 the value for t_hotelticket.addressline1
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setAddressline1(String addressline1) {
        this.addressline1 = addressline1 == null ? null : addressline1.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.addressline2
     *
     * @return the value of t_hotelticket.addressline2
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getAddressline2() {
        return addressline2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.addressline2
     *
     * @param addressline2 the value for t_hotelticket.addressline2
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setAddressline2(String addressline2) {
        this.addressline2 = addressline2 == null ? null : addressline2.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.zipcode
     *
     * @return the value of t_hotelticket.zipcode
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.zipcode
     *
     * @param zipcode the value for t_hotelticket.zipcode
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode == null ? null : zipcode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.city
     *
     * @return the value of t_hotelticket.city
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getCity() {
        return city;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.city
     *
     * @param city the value for t_hotelticket.city
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.state
     *
     * @return the value of t_hotelticket.state
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.state
     *
     * @param state the value for t_hotelticket.state
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.country
     *
     * @return the value of t_hotelticket.country
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getCountry() {
        return country;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.country
     *
     * @param country the value for t_hotelticket.country
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.countryisocode
     *
     * @return the value of t_hotelticket.countryisocode
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getCountryisocode() {
        return countryisocode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.countryisocode
     *
     * @param countryisocode the value for t_hotelticket.countryisocode
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setCountryisocode(String countryisocode) {
        this.countryisocode = countryisocode == null ? null : countryisocode.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.star_rating
     *
     * @return the value of t_hotelticket.star_rating
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getStarRating() {
        return starRating;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.star_rating
     *
     * @param starRating the value for t_hotelticket.star_rating
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setStarRating(String starRating) {
        this.starRating = starRating == null ? null : starRating.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.longitude
     *
     * @return the value of t_hotelticket.longitude
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getLongitude() {
        return longitude;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.longitude
     *
     * @param longitude the value for t_hotelticket.longitude
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude == null ? null : longitude.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.latitude
     *
     * @return the value of t_hotelticket.latitude
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.latitude
     *
     * @param latitude the value for t_hotelticket.latitude
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude == null ? null : latitude.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.url
     *
     * @return the value of t_hotelticket.url
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getUrl() {
        return url;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.url
     *
     * @param url the value for t_hotelticket.url
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.checkin
     *
     * @return the value of t_hotelticket.checkin
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getCheckin() {
        return checkin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.checkin
     *
     * @param checkin the value for t_hotelticket.checkin
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setCheckin(String checkin) {
        this.checkin = checkin == null ? null : checkin.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.checkout
     *
     * @return the value of t_hotelticket.checkout
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getCheckout() {
        return checkout;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.checkout
     *
     * @param checkout the value for t_hotelticket.checkout
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setCheckout(String checkout) {
        this.checkout = checkout == null ? null : checkout.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.numberrooms
     *
     * @return the value of t_hotelticket.numberrooms
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getNumberrooms() {
        return numberrooms;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.numberrooms
     *
     * @param numberrooms the value for t_hotelticket.numberrooms
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setNumberrooms(String numberrooms) {
        this.numberrooms = numberrooms == null ? null : numberrooms.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.numberfloors
     *
     * @return the value of t_hotelticket.numberfloors
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getNumberfloors() {
        return numberfloors;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.numberfloors
     *
     * @param numberfloors the value for t_hotelticket.numberfloors
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setNumberfloors(String numberfloors) {
        this.numberfloors = numberfloors == null ? null : numberfloors.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.yearopened
     *
     * @return the value of t_hotelticket.yearopened
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getYearopened() {
        return yearopened;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.yearopened
     *
     * @param yearopened the value for t_hotelticket.yearopened
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setYearopened(String yearopened) {
        this.yearopened = yearopened == null ? null : yearopened.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.yearrenovated
     *
     * @return the value of t_hotelticket.yearrenovated
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getYearrenovated() {
        return yearrenovated;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.yearrenovated
     *
     * @param yearrenovated the value for t_hotelticket.yearrenovated
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setYearrenovated(String yearrenovated) {
        this.yearrenovated = yearrenovated == null ? null : yearrenovated.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.photo1
     *
     * @return the value of t_hotelticket.photo1
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getPhoto1() {
        return photo1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.photo1
     *
     * @param photo1 the value for t_hotelticket.photo1
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setPhoto1(String photo1) {
        this.photo1 = photo1 == null ? null : photo1.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.photo2
     *
     * @return the value of t_hotelticket.photo2
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getPhoto2() {
        return photo2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.photo2
     *
     * @param photo2 the value for t_hotelticket.photo2
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setPhoto2(String photo2) {
        this.photo2 = photo2 == null ? null : photo2.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.photo3
     *
     * @return the value of t_hotelticket.photo3
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getPhoto3() {
        return photo3;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.photo3
     *
     * @param photo3 the value for t_hotelticket.photo3
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setPhoto3(String photo3) {
        this.photo3 = photo3 == null ? null : photo3.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.photo4
     *
     * @return the value of t_hotelticket.photo4
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getPhoto4() {
        return photo4;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.photo4
     *
     * @param photo4 the value for t_hotelticket.photo4
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setPhoto4(String photo4) {
        this.photo4 = photo4 == null ? null : photo4.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.photo5
     *
     * @return the value of t_hotelticket.photo5
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getPhoto5() {
        return photo5;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.photo5
     *
     * @param photo5 the value for t_hotelticket.photo5
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setPhoto5(String photo5) {
        this.photo5 = photo5 == null ? null : photo5.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.overview
     *
     * @return the value of t_hotelticket.overview
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getOverview() {
        return overview;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.overview
     *
     * @param overview the value for t_hotelticket.overview
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setOverview(String overview) {
        this.overview = overview == null ? null : overview.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.rates_from
     *
     * @return the value of t_hotelticket.rates_from
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getRatesFrom() {
        return ratesFrom;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.rates_from
     *
     * @param ratesFrom the value for t_hotelticket.rates_from
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setRatesFrom(String ratesFrom) {
        this.ratesFrom = ratesFrom == null ? null : ratesFrom.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.continent_id
     *
     * @return the value of t_hotelticket.continent_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getContinentId() {
        return continentId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.continent_id
     *
     * @param continentId the value for t_hotelticket.continent_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setContinentId(String continentId) {
        this.continentId = continentId == null ? null : continentId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.continent_name
     *
     * @return the value of t_hotelticket.continent_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getContinentName() {
        return continentName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.continent_name
     *
     * @param continentName the value for t_hotelticket.continent_name
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setContinentName(String continentName) {
        this.continentName = continentName == null ? null : continentName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.city_id
     *
     * @return the value of t_hotelticket.city_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getCityId() {
        return cityId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.city_id
     *
     * @param cityId the value for t_hotelticket.city_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setCityId(String cityId) {
        this.cityId = cityId == null ? null : cityId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.country_id
     *
     * @return the value of t_hotelticket.country_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.country_id
     *
     * @param countryId the value for t_hotelticket.country_id
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId == null ? null : countryId.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.number_of_reviews
     *
     * @return the value of t_hotelticket.number_of_reviews
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getNumberOfReviews() {
        return numberOfReviews;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.number_of_reviews
     *
     * @param numberOfReviews the value for t_hotelticket.number_of_reviews
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setNumberOfReviews(String numberOfReviews) {
        this.numberOfReviews = numberOfReviews == null ? null : numberOfReviews.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.rating_average
     *
     * @return the value of t_hotelticket.rating_average
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getRatingAverage() {
        return ratingAverage;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.rating_average
     *
     * @param ratingAverage the value for t_hotelticket.rating_average
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setRatingAverage(String ratingAverage) {
        this.ratingAverage = ratingAverage == null ? null : ratingAverage.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_hotelticket.rates_currency
     *
     * @return the value of t_hotelticket.rates_currency
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public String getRatesCurrency() {
        return ratesCurrency;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_hotelticket.rates_currency
     *
     * @param ratesCurrency the value for t_hotelticket.rates_currency
     *
     * @mbg.generated Sun Oct 20 13:50:38 CST 2019
     */
    public void setRatesCurrency(String ratesCurrency) {
        this.ratesCurrency = ratesCurrency == null ? null : ratesCurrency.trim();
    }
}