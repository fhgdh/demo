package com.example.demo.bean;

public class Admin {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_admin.admin_id
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    private Integer adminId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_admin.admin_name
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    private String adminName;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_admin.admin_password
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    private String adminPassword;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column t_admin.admin_nickname
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    private String adminNickname;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_admin.admin_id
     *
     * @return the value of t_admin.admin_id
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public Integer getAdminId() {
        return adminId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_admin.admin_id
     *
     * @param adminId the value for t_admin.admin_id
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_admin.admin_name
     *
     * @return the value of t_admin.admin_name
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public String getAdminName() {
        return adminName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_admin.admin_name
     *
     * @param adminName the value for t_admin.admin_name
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public void setAdminName(String adminName) {
        this.adminName = adminName == null ? null : adminName.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_admin.admin_password
     *
     * @return the value of t_admin.admin_password
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public String getAdminPassword() {
        return adminPassword;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_admin.admin_password
     *
     * @param adminPassword the value for t_admin.admin_password
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword == null ? null : adminPassword.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column t_admin.admin_nickname
     *
     * @return the value of t_admin.admin_nickname
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public String getAdminNickname() {
        return adminNickname;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column t_admin.admin_nickname
     *
     * @param adminNickname the value for t_admin.admin_nickname
     *
     * @mbg.generated Thu Oct 31 11:26:21 CST 2019
     */
    public void setAdminNickname(String adminNickname) {
        this.adminNickname = adminNickname == null ? null : adminNickname.trim();
    }
}