package net.suncaper.hostel.service;

import net.suncaper.hostel.domain.Message;
import net.suncaper.hostel.domain.MessageExample;
import net.suncaper.hostel.mapper.MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public abstract class MessageServiceImpl implements MessageService{

    @Autowired
    MessageMapper messageMapper;

    @Override
    public List<Message> findMessagebyhotel(int  hotel_id) {
        MessageExample messageExample=new MessageExample();
        messageExample.createCriteria().andHotelIdEqualTo(hotel_id);
        List<Message> messages =messageMapper.selectByExample(messageExample);
        return messages;
    }

    @Override
    public void insertMessage(int hotel_id,String message,String message_user_name,String message_room_name) {
        messageMapper.insert(hotel_id,message,message_user_name,message_room_name);
    }
}
