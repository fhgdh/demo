package com.example.demo.service.impl;

import com.example.demo.bean.*;
import com.example.demo.bean.Hotel;
import com.example.demo.mapper.HotelMapper;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.mapper.RoomMapper;
import com.example.demo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceimpl implements OrderService {
    @Autowired
    RoomMapper roomMapper;
    @Autowired
    HotelMapper hotelMapper;
    @Autowired
    OrderMapper orderMapper;

    @Override
    public List<Room> getRoomInfo(Room room) {
        RoomExample roomExample = new RoomExample();
        roomExample.createCriteria().andRoomIdEqualTo(room.getRoomId());
        List<Room> rooms = roomMapper.selectByExample(roomExample);
        return rooms;
    }

    @Override
    public Hotel getHotelInfo(Integer hotelId) {
        return hotelMapper.selectByPrimaryKey(hotelId);
    }

    @Override
    public void insertOrder(Order order) {
        orderMapper.insertSelective(order);
    }

    @Override
    public void updateOrderStatus(Order order) {
        OrderExample orderExample = new OrderExample();
        orderExample.createCriteria().andOrderIdEqualTo(order.getOrderId());
        orderMapper.updateByExampleSelective(order, orderExample);
    }
}
