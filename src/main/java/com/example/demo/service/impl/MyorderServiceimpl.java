package com.example.demo.service.impl;

import com.example.demo.bean.*;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.service.MyorderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class MyorderServiceimpl  implements MyorderService {
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    UserMapper userMapper;


    @Override
    public List<User> getUserInfo(User user) {
        UserExample userExample = new UserExample();
        userExample.createCriteria().andUserIdEqualTo(user.getUserId());
        List<User> users = userMapper.selectByExample(userExample);
        return users;
    }

    @Override
    public List<Order> getOrderInfo(Integer userId) {
        OrderExample orderExample = new OrderExample();
        orderExample.createCriteria().andUserIdEqualTo(userId);
        List<Order> orders = orderMapper.selectByExample(orderExample);
        return orders;
    }

    @Override
    public Order getMyorder(Integer orderId) {
      Order od= orderMapper.selectByPrimaryKey(orderId);
        System.out.println(od.toString());
        return  od;
    }
}

