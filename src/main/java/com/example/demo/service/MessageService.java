package net.suncaper.hostel.service;

import net.suncaper.hostel.domain.Message;

import java.util.List;

public interface MessageService {
    public List<Message> findMessagebyhotel(int  hotel_id);
    public void insertMessage(int hotel_id,String message,String message_user_name,String message_room_name);
}
