package com.example.demo.service;

import com.example.demo.bean.Order;
import com.example.demo.bean.Hotel;
import com.example.demo.bean.Room;

import java.util.List;

public interface OrderService {

    List<Room> getRoomInfo(Room room);

    Hotel getHotelInfo(Integer hotelId);

    void insertOrder(Order order);

    void updateOrderStatus(Order order);
}
