package com.example.demo.service;


import com.example.demo.bean.Admin;

public interface AdminService {
    public Admin findAdminbyName(String Name);
}
