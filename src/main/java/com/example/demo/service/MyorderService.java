package com.example.demo.service;

import com.example.demo.bean.Order;
import com.example.demo.bean.User;
import com.example.demo.mapper.HotelMapper;
import com.example.demo.mapper.OrderMapper;
import com.example.demo.mapper.RoomMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public interface MyorderService {

      Order getMyorder(Integer orderId);

     List<User> getUserInfo(User user);

     List<Order> getOrderInfo(Integer userId);
}

