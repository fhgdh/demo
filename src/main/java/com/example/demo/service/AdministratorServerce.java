package com.example.demo.service;

import com.example.demo.dao.AdministratorDao;
import com.example.demo.dao.SelectHotellInformationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("AdministratorServerce")
@Transactional
public class AdministratorServerce {

    @Autowired
    AdministratorDao administratorDao;

    public List<Map> findLike(String keWords)
    {
        return administratorDao.findLike(keWords);
    }

    public int agree(int order_id)
    {
        return administratorDao.agree(order_id);
    }

    public int disagree(int order_id)
    {
        return administratorDao.disagree(order_id);
    }
    public List<Map> allRoom()
    {
        return administratorDao.allRoom();
    }


    public int updateById(Map map)
        {
            return administratorDao.updateById(map);
        }
}
