package com.example.demo.service;



import com.example.demo.bean.Admin;
import com.example.demo.bean.AdminExample;
import com.example.demo.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
@Service
public class AdminServiceImpl implements AdminService{
    @Autowired
    AdminMapper adminMapper;

    @Override
    public Admin findAdminbyName(String Name) {
        AdminExample example = new AdminExample();
        example.createCriteria().andAdminNameEqualTo(Name);
        List<Admin> admins = adminMapper.selectByExample(example);
        return admins.size() > 0 ? admins.get(0) : null;
    }
}
