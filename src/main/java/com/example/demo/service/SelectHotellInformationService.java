package com.example.demo.service;

import com.example.demo.dao.SelectHotellInformationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("SelectHotellInformationService")
@Transactional
public class SelectHotellInformationService {
	@Autowired
	private SelectHotellInformationDao dao;
	
	public List<Map> selectRoomByHotellID(Integer hotel_id) {
		// TODO Auto-generated method stub
//		System.out.println("do have something");
		return dao.selectRoomByHotellID(hotel_id);
	}
	public List<Map> selectRoom(Map map) {
		// TODO Auto-generated method stub
		System.out.println("do have something");
		return dao.selectRoom(map);
	}
}
